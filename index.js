const express = require('express')
const app = express()
const port = 8021
const cors = require('cors')
const fs = require('fs'); 
const { json } = require('express/lib/response');
const fsPromises = require('fs').promises;

var bodyParser = require('body-parser');
const { checkPrimeSync } = require('crypto');

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))
// parse application/json
app.use(bodyParser.json()) 
app.use(cors());
//app.use(bodyParser.json());

const orders = {};

async function wrapper(){
  const products = JSON.parse(await fs.promises.readFile("./test/products.json", 'utf-8'));
  let cartBuffer = {};
  app.get('/products', (req, res, next) => {
    res.json(products.response.data);
    res.end();
  })

  app.get('/products/:productId', (req, res, next) => {
    fs.promises.readFile("./test/products.json", 'utf-8')
    .then(function (productsJson) {
      const productsObject = JSON.parse(productsJson);
      res.json(productsObject.response.data[req.params.productId]);
      res.end();
    })
    .catch(function (error) {
      res.json({
        name: "404",
        type: "no data available"
      })
      res.end();
    });
  })

  app.post('/order',(req, res) => {
    let sql = "INSERT INTO users SET ?";
    const newId = Math.floor(Math.random()*90000) + 10000;
    orders[newId] = req.body;
    res.send(JSON.stringify({"status": 200, "error": null, "response": "results"}));
    res.end();
  });

  app.get('/history',(req, res) => {
    let sql = "INSERT INTO users SET ?";
    //console.log(req.body);
    res.json(orders);
    res.end();
  });

   app.get('/history/:orderId',(req, res) => {
     let sql = "INSERT INTO users SET ?";
     if(orders[req.params.orderId]){
       const result = Object.entries(orders[req.params.orderId])
       .reduce((orderDetails, product) => {
         const result = {name: products.response.data[product[0]]['name'],
         price: products.response.data[product[0]]['price'],
       quantity: product[1]};
         orderDetails.push(result);
          return orderDetails;
        }, []);
      res.json(result);
     } else{
       res.json({});
     }
     res.end();
   });


   app.get('/cart/:productId',(req, res) => {
     if(!cartBuffer[req.params.productId]){
       res.json(0);
     } else{
       res.json(cartBuffer[req.params.productId])
     }
     //console.log(cartBuffer);
     res.end();
     });

     app.get('/cart',(req, res) => {
      res.json(cartBuffer);
      //console.log(cartBuffer);
      res.end();
      });

      app.post('/cart',(req, res) => {
        cartBuffer = {};
        console.log(cartBuffer);
      });


  app.post('/cart/:productId',(req, res) => {
    cartBuffer[req.params.productId] = req.body.quantity;
    console.log(cartBuffer);
  });
}

wrapper();



app.use(express.static('public'))
app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
}) 
